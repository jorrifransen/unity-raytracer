using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class RayTracingMaster : MonoBehaviour
{
    public ComputeShader ray_tracing_shader;
    public Texture SkyboxTexture;
    public Light DirectionalLight;

    private RenderTexture _target;
    private Camera _camera;
    private uint _currentSample = 0;
    private Material _addMaterial;

    private bool _running = true;
    public uint CurrentBounceCount = 2;

    public Vector2 SphereRadius = new Vector2(3.0f, 8.0f);
    public uint SpheresMax = 100;
    public float SpherePlacementRadius = 100.0f;
    private ComputeBuffer _sphereBuffer;

    private RenderTexture _converged;

    public int SphereSeed = 0;

    public struct Sphere
    {
        public Vector3 position;
        public float radius;

        public Vector3 albedo;
        public Vector3 specular;
        public float smoothness;
        public Vector3 emission;
    }

    private void Awake()
    {
        _camera = GetComponent<Camera>();

        var shader = Shader.Find("Hidden/AddShader");
        Debug.Assert(shader);
        _addMaterial = new Material(shader);

        Debug.Assert(_camera);
        Debug.Assert(_addMaterial);
    }

    private void OnEnable()
    {
        _currentSample = 0;
        SetUpScene();
    }

    private void OnDisable()
    {
        if (_sphereBuffer != null) {
            _sphereBuffer.Release();
        }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture dest)
    {
        if (_running) {
            SetShaderParameters();
            Render(dest);
        }
    }

    private void SetUpScene()
    {
        Random.InitState(SphereSeed);

        List<Sphere> spheres = new List<Sphere>();

        for (int i = 0; i < SpheresMax; i++)
        {
            Sphere sphere = new Sphere();

            sphere.radius = SphereRadius.x + Random.value * (SphereRadius.y - SphereRadius.x);
            Vector2 randomPos = Random.insideUnitCircle * SpherePlacementRadius;
            sphere.position = new Vector3(randomPos.x, sphere.radius, randomPos.y);

            // Reject spheres that are intersecting others
            foreach (Sphere other in spheres) {
                float minDist = sphere.radius + other.radius;
                if (Vector3.SqrMagnitude(sphere.position - other.position) < minDist * minDist) {
                    goto SkipSphere;
                }
            }

            Color color = Random.ColorHSV();
            float chance = Random.value;
            if (chance < 0.8f) {
                bool metal = chance < 0.4f;
                sphere.albedo = metal ? Vector4.zero : new Vector4(color.r, color.g, color.b);
                sphere.specular = metal ? new Vector4(color.r, color.g, color.b) : new Vector4(0.04f, 0.04f, 0.04f);
                sphere.smoothness = Random.value;
            } else {
                Color emission = Random.ColorHSV(0, 1, 0, 1, 3.0f, 8.0f);
                sphere.emission = new Vector3(emission.r, emission.g, emission.b);
            }


            // Color color = Random.ColorHSV();
            // bool metal = Random.value < 0.4f;
            // sphere.albedo = metal ? Vector3.zero : new Vector3(color.r, color.g, color.b);
            // sphere.specular = metal ? new Vector3(color.r, color.g, color.b) :
            //                           Vector3.one * 0.06f * Random.value;
            // Color color = Random.ColorHSV();
            // bool metal = Random.value < 0.5f;
            // sphere.albedo = metal ? Vector4.zero : new Vector4(color.r, color.g, color.b);
            // sphere.specular = metal ? new Vector4(color.r, color.g, color.b) : Vector4.one * 0.04f;

            spheres.Add(sphere);

SkipSphere:
            continue;
        }

        // Debug.Log("Sizeof(sphere): " + System.Runtime.InteropServices.Marshal.SizeOf<Sphere>());
        var sphere_byte_size = Marshal.SizeOf<Sphere>();

        _sphereBuffer = new ComputeBuffer(spheres.Count, sphere_byte_size);
        _sphereBuffer.SetData(spheres);
    }

    private void Render(RenderTexture dest)
    {
        InitRenderTexture();

        ray_tracing_shader.SetTexture(0, "Result", _target);

        int thread_groups_x = Mathf.CeilToInt(Screen.width / 8.0f);
        int thread_groups_y = Mathf.CeilToInt(Screen.height / 8.0f);

        ray_tracing_shader.Dispatch(0, thread_groups_x, thread_groups_y, 1);

        _addMaterial.SetFloat("_Sample", _currentSample);
        Graphics.Blit(_target, _converged, _addMaterial);
        Graphics.Blit(_converged, dest);
        _currentSample++;
    }

    private void InitRenderTexture()
    {
        if (_target == null || _target.width != Screen.width || _target.height != Screen.height) {
            if (_target != null) {
                _target.Release();
            }

            _target = new RenderTexture(Screen.width, Screen.height, 0,
                                        RenderTextureFormat.ARGBFloat,
                                        RenderTextureReadWrite.Linear);
            _target.enableRandomWrite = true;
            _target.Create();
            _currentSample = 0;
        }

        if (_converged == null || _converged.width != Screen.width || _target.height != Screen.height) {
            if (_converged != null) {
                _converged.Release();
            }

            _converged = new RenderTexture(Screen.width, Screen.height, 0,
                                           RenderTextureFormat.ARGBFloat,
                                           RenderTextureReadWrite.Linear);

            _converged.enableRandomWrite = true;
            _converged.Create();
            _currentSample = 0;
        }
    }

    private void SetShaderParameters()
    {
        ray_tracing_shader.SetTexture(0, "_SkyboxTexture", SkyboxTexture);
        ray_tracing_shader.SetMatrix("_CameraToWorld", _camera.cameraToWorldMatrix);
        ray_tracing_shader.SetMatrix("_CameraInverseProjection", _camera.projectionMatrix.inverse);
        ray_tracing_shader.SetVector("_PixelOffset", new Vector2(Random.value, Random.value));
        ray_tracing_shader.SetInt("_BounceCount", (int)CurrentBounceCount);

        Vector3 l = DirectionalLight.transform.forward;
        ray_tracing_shader.SetVector("_DirectionalLight",
                                     new Vector4(l.x, l.y, l.z, DirectionalLight.intensity));

        ray_tracing_shader.SetBuffer(0, "_Spheres", _sphereBuffer);

        ray_tracing_shader.SetFloat("_Seed", Random.value);
    }

    void Start()
    {
    }

    void Update()
    {
        if (transform.hasChanged) {
            _currentSample = 0;
            transform.hasChanged = false;
        }

        if (DirectionalLight.transform.hasChanged) {
            _currentSample = 0;
            DirectionalLight.transform.hasChanged = false;
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            _currentSample = 0;
            _running = !_running;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            _currentSample = 0;
            CurrentBounceCount += 1;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            _currentSample = 0;
            CurrentBounceCount -= 1;
        }

        if (CurrentBounceCount < 1) CurrentBounceCount = 1;
    }
}
